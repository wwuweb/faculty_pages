<?php
/**
 * @file
 * faculty_pages.features.inc
 */

/**
 * Implements hook_views_api().
 */
function faculty_pages_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function faculty_pages_node_info() {
  $items = array(
    'faculty_page' => array(
      'name' => t('Faculty Page'),
      'base' => 'node_content',
      'description' => t('Add additional pages to faculty profiles with a <em>Faculty Page</em>.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
