<?php
/**
 * @file
 * faculty_pages.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function faculty_pages_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view_panel_context_6';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -30;
  $handler->conf = array(
    'title' => 'Faculty Interior Page',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(
      0 => array(
        'identifier' => 'User from Node (on node.node_author)',
        'keyword' => 'user',
        'name' => 'entity_from_schema:uid-node-user',
        'context' => 'argument_entity_id:node_1',
        'id' => 1,
      ),
    ),
    'access' => array(
      'logic' => 'and',
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'faculty_page' => 'faculty_page',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
    ),
  );
  $display = new panels_display();
  $display->layout = 'clean-25-75';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'left' => NULL,
      'right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%user:field-professional-title %user:field_preferred_name %user:field_last_name';
  $display->uuid = 'b8325d61-edb4-45a3-acc5-be4454387745';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-a8d3f2e4-7115-4fff-8fea-80e22829b081';
    $pane->panel = 'left';
    $pane->type = 'entity_field';
    $pane->subtype = 'user:field_your_photo';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'image',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'image_style' => '',
        'image_link' => '',
      ),
      'context' => 'relationship_entity_from_schema:uid-node-user_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'a8d3f2e4-7115-4fff-8fea-80e22829b081';
    $display->content['new-a8d3f2e4-7115-4fff-8fea-80e22829b081'] = $pane;
    $display->panels['left'][0] = 'new-a8d3f2e4-7115-4fff-8fea-80e22829b081';
    $pane = new stdClass();
    $pane->pid = 'new-6e13ce16-e34e-42f7-ad0b-dff37b34600d';
    $pane->panel = 'left';
    $pane->type = 'views';
    $pane->subtype = 'faculty_pages_menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_menu',
      'context' => array(
        0 => 'argument_entity_id:node_1.author',
      ),
      'override_title' => 1,
      'override_title_text' => '<none>',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '6e13ce16-e34e-42f7-ad0b-dff37b34600d';
    $display->content['new-6e13ce16-e34e-42f7-ad0b-dff37b34600d'] = $pane;
    $display->panels['left'][1] = 'new-6e13ce16-e34e-42f7-ad0b-dff37b34600d';
    $pane = new stdClass();
    $pane->pid = 'new-c63a0161-4fd1-4a19-bb81-f098fb75fe35';
    $pane->panel = 'left';
    $pane->type = 'views';
    $pane->subtype = 'faculty_pages_menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_1',
      'context' => array(
        0 => 'argument_entity_id:node_1.author',
      ),
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'c63a0161-4fd1-4a19-bb81-f098fb75fe35';
    $display->content['new-c63a0161-4fd1-4a19-bb81-f098fb75fe35'] = $pane;
    $display->panels['left'][2] = 'new-c63a0161-4fd1-4a19-bb81-f098fb75fe35';
    $pane = new stdClass();
    $pane->pid = 'new-955d5a65-c8f4-4d75-9169-4fac79189d0b';
    $pane->panel = 'right';
    $pane->type = 'node_content';
    $pane->subtype = 'node_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'links' => 0,
      'no_extras' => 0,
      'override_title' => 0,
      'override_title_text' => '',
      'identifier' => '',
      'link' => 0,
      'leave_node_title' => 0,
      'build_mode' => 'full',
      'context' => 'argument_entity_id:node_1',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '955d5a65-c8f4-4d75-9169-4fac79189d0b';
    $display->content['new-955d5a65-c8f4-4d75-9169-4fac79189d0b'] = $pane;
    $display->panels['right'][0] = 'new-955d5a65-c8f4-4d75-9169-4fac79189d0b';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['node_view_panel_context_6'] = $handler;

  return $export;
}
