Setup instructions:

1. The url aliases for Faculty Pages should be set to include the node author in addition to the node title - e.g. [node:author:name]/[node:title]
2. The url aliases for Users must reflect the path for the Faculty Pages Menu view. This presents two options:
	-Change the path of the Faculty Pages Menu view. By default this is faculty/%/menu. 'faculty' may be changed to any desired path, as long as the context remains in the second parameter followed by 'menu'.
	-Change the path of Users to faculty/[user:name]
3. Faculty pages are associated with a user based on the node author. As such, users should create their own pages when possible. When not possible, the author of a faculty page must be manually set. 