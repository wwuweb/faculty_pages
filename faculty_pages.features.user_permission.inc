<?php
/**
 * @file
 * faculty_pages.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function faculty_pages_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access draggableviews'.
  $permissions['access draggableviews'] = array(
    'name' => 'access draggableviews',
    'roles' => array(
      'Contributor' => 'Contributor',
      'Faculty' => 'Faculty',
      'Super Editor' => 'Super Editor',
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'draggableviews',
  );

  // Exported permission: 'create faculty_page content'.
  $permissions['create faculty_page content'] = array(
    'name' => 'create faculty_page content',
    'roles' => array(
      'Faculty' => 'Faculty',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any faculty_page content'.
  $permissions['delete any faculty_page content'] = array(
    'name' => 'delete any faculty_page content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own faculty_page content'.
  $permissions['delete own faculty_page content'] = array(
    'name' => 'delete own faculty_page content',
    'roles' => array(
      'Contributor' => 'Contributor',
      'Faculty' => 'Faculty',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any faculty_page content'.
  $permissions['edit any faculty_page content'] = array(
    'name' => 'edit any faculty_page content',
    'roles' => array(
      'Computer Science Editor' => 'Computer Science Editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own faculty_page content'.
  $permissions['edit own faculty_page content'] = array(
    'name' => 'edit own faculty_page content',
    'roles' => array(
      'Faculty' => 'Faculty',
    ),
    'module' => 'node',
  );

  return $permissions;
}
